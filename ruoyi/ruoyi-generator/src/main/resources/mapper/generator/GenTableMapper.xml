<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.ruoyi.generator.mapper.GenTableMapper">
    <resultMap type="GenTable" id="GenTableResult">
        <id property="tableId" column="table_id"/>
        <result property="tableName" column="table_name"/>
        <result property="tableComment" column="table_comment"/>
        <result property="subTableName" column="sub_table_name"/>
        <result property="subTableFkName" column="sub_table_fk_name"/>
        <result property="className" column="class_name"/>
        <result property="tplCategory" column="tpl_category"/>
        <result property="packageName" column="package_name"/>
        <result property="moduleName" column="module_name"/>
        <result property="businessName" column="business_name"/>
        <result property="functionName" column="function_name"/>
        <result property="functionAuthor" column="function_author"/>
        <result property="genType" column="gen_type"/>
        <result property="genPath" column="gen_path"/>
        <result property="options" column="options"/>
        <result property="createBy" column="create_by"/>
        <result property="createTime" column="create_time"/>
        <result property="updateBy" column="update_by"/>
        <result property="updateTime" column="update_time"/>
        <result property="remark" column="remark"/>
        <result property="swagger" column="swagger"/>
        <result property="excelExport" column="excel_export"/>
        <result property="vueVersion" column="vue_version"/>
        <collection property="columns" javaType="java.util.List" resultMap="GenTableColumnResult"/>
    </resultMap>

    <resultMap type="GenTableColumn" id="GenTableColumnResult">
        <id property="columnId" column="column_id"/>
        <result property="tableId" column="table_id"/>
        <result property="columnName" column="column_name"/>
        <result property="columnComment" column="column_comment"/>
        <result property="columnType" column="column_type"/>
        <result property="javaType" column="java_type"/>
        <result property="javaField" column="java_field"/>
        <result property="isPk" column="is_pk"/>
        <result property="isIncrement" column="is_increment"/>
        <result property="isRequired" column="is_required"/>
        <result property="isInsert" column="is_insert"/>
        <result property="isEdit" column="is_edit"/>
        <result property="isList" column="is_list"/>
        <result property="isQuery" column="is_query"/>
        <result property="queryType" column="query_type"/>
        <result property="htmlType" column="html_type"/>
        <result property="dictType" column="dict_type"/>
        <result property="sort" column="sort"/>
        <result property="createBy" column="create_by"/>
        <result property="createTime" column="create_time"/>
        <result property="updateBy" column="update_by"/>
        <result property="updateTime" column="update_time"/>
    </resultMap>

    <sql id="selectGenTableVo">
        SELECT table_id,
               table_name,
               table_comment,
               sub_table_name,
               sub_table_fk_name,
               class_name,
               tpl_category,
               package_name,
               module_name,
               business_name,
               function_name,
               function_author,
               gen_type,
               gen_path,
               options,
               create_by,
               create_time,
               update_by,
               update_time,
               remark,
               swagger,
               excel_export,
               vue_version
        FROM gen_table
    </sql>

    <select id="selectGenTableList" parameterType="GenTable" resultMap="GenTableResult">
        <include refid="selectGenTableVo"/>
        <where>
            <if test="tableName != null and tableName != ''">
                LOWER(table_name) LIKE LOWER(CONCAT('%', #{tableName}, '%'))
            </if>
            <if test="tableComment != null and tableComment != ''">
                AND LOWER(table_comment) LIKE LOWER(CONCAT('%', #{tableComment}, '%'))
            </if>
            <if test="swagger != null">
                AND swagger = #{swagger,jdbcType=INTEGER}
            </if>
            <if test="excelExport != null">
                AND excel_export = #{excelExport,jdbcType=INTEGER}
            </if>
            <if test="vueVersion != null">
                AND vue_version = #{vueVersion,jdbcType=INTEGER}
            </if>
            <if test="params.beginTime != null and params.beginTime != ''">
                <!-- 开始时间检索 -->
                AND DATE_FORMAT(create_time, '%y%m%d') &gt;= DATE_FORMAT(#{params.beginTime,jdbcType=VARCHAR}, '%y%m%d')
            </if>
            <if test="params.endTime != null and params.endTime != ''">
                <!-- 结束时间检索 -->
                AND DATE_FORMAT(create_time, '%y%m%d') &lt;= DATE_FORMAT(#{params.endTime,jdbcType=VARCHAR}, '%y%m%d')
            </if>
            <if test="params.beginUpdateTime != null and params.beginUpdateTime != '' and params.endUpdateTime != null and params.endUpdateTime != ''">
                AND DATE_FORMAT(update_time, '%y%m%d') BETWEEN
                        DATE_FORMAT(#{params.beginUpdateTime,jdbcType=VARCHAR}, '%y%m%d')
                        AND
                        DATE_FORMAT(#{params.endUpdateTime,jdbcType=VARCHAR}, '%y%m%d')
            </if>
        </where>
        ORDER BY table_id DESC
    </select>

    <select id="selectDbTableList" parameterType="GenTable" resultMap="GenTableResult">
        SELECT table_name, table_comment, create_time, update_time
        FROM information_schema.tables
                WHERE table_schema = (SELECT DATABASE())
                  AND table_name NOT LIKE 'qrtz_%'
                  AND table_name NOT LIKE 'gen_%'
                  AND table_name NOT IN (SELECT table_name FROM gen_table)
        <if test="tableName != null and tableName != ''">
            AND LOWER(table_name) LIKE LOWER(CONCAT('%', #{tableName,jdbcType=VARCHAR}, '%'))
        </if>
        <if test="tableComment != null and tableComment != ''">
            AND LOWER(table_comment) LIKE LOWER(CONCAT('%', #{tableComment,jdbcType=VARCHAR}, '%'))
        </if>
        <if test="params.beginTime != null and params.beginTime != ''">
            <!-- 开始时间检索 -->
            AND DATE_FORMAT(create_time, '%y%m%d') &gt;= DATE_FORMAT(#{params.beginTime,jdbcType=VARCHAR}, '%y%m%d')
        </if>
        <if test="params.endTime != null and params.endTime != ''">
            <!-- 结束时间检索 -->
            AND DATE_FORMAT(create_time, '%y%m%d') &lt;= DATE_FORMAT(#{params.endTime,jdbcType=VARCHAR}, '%y%m%d')
        </if>
    </select>

    <select id="selectDbTableListByNames" resultMap="GenTableResult">
        SELECT table_name, table_comment, create_time, update_time
        FROM information_schema.tables
                WHERE table_name NOT LIKE 'qrtz_%'
                  AND table_name NOT LIKE 'gen_%'
                  AND table_schema = (SELECT DATABASE())
                  AND table_name IN
        <foreach collection="array" item="name" open="(" separator="," close=")">
            #{name,jdbcType=VARCHAR}
        </foreach>
    </select>

    <select id="selectTableByName" parameterType="String" resultMap="GenTableResult">
        SELECT table_name, table_comment, create_time, update_time
        FROM information_schema.tables
        WHERE table_comment <![CDATA[ <> ]]> ''
          AND table_schema = (SELECT DATABASE())
          AND table_name = #{tableName,jdbcType=VARCHAR}
    </select>

    <select id="selectGenTableById" parameterType="Long" resultMap="GenTableResult">
        SELECT t.table_id,
               t.table_name,
               t.table_comment,
               t.sub_table_name,
               t.sub_table_fk_name,
               t.class_name,
               t.tpl_category,
               t.package_name,
               t.module_name,
               t.business_name,
               t.function_name,
               t.function_author,
               t.gen_type,
               t.gen_path,
               t.options,
               t.remark,
               t.swagger,
               t.excel_export,
               t.vue_version,
               c.column_id,
               c.column_name,
               c.column_comment,
               c.column_type,
               c.java_type,
               c.java_field,
               c.is_pk,
               c.is_increment,
               c.is_required,
               c.is_insert,
               c.is_edit,
               c.is_list,
               c.is_query,
               c.query_type,
               c.html_type,
               c.dict_type,
               c.sort
        FROM gen_table t
                     LEFT JOIN gen_table_column c ON t.table_id = c.table_id
        where t.table_id = #{tableId}
        ORDER BY c.sort
    </select>

    <select id="selectGenTableByName" parameterType="String" resultMap="GenTableResult">
        SELECT t.table_id,
               t.table_name,
               t.table_comment,
               t.sub_table_name,
               t.sub_table_fk_name,
               t.class_name,
               t.tpl_category,
               t.package_name,
               t.module_name,
               t.business_name,
               t.function_name,
               t.function_author,
               t.gen_type,
               t.gen_path,
               t.options,
               t.remark,
               t.swagger,
               t.excel_export,
               t.vue_version,
               c.column_id,
               c.column_name,
               c.column_comment,
               c.column_type,
               c.java_type,
               c.java_field,
               c.is_pk,
               c.is_increment,
               c.is_required,
               c.is_insert,
               c.is_edit,
               c.is_list,
               c.is_query,
               c.query_type,
               c.html_type,
               c.dict_type,
               c.sort
        FROM gen_table t
                     LEFT JOIN gen_table_column c ON t.table_id = c.table_id
        WHERE t.table_name = #{tableName,jdbcType=VARCHAR}
        ORDER BY c.sort
    </select>

    <select id="selectGenTableAll" parameterType="String" resultMap="GenTableResult">
        SELECT t.table_id,
               t.table_name,
               t.table_comment,
               t.sub_table_name,
               t.sub_table_fk_name,
               t.class_name,
               t.tpl_category,
               t.package_name,
               t.module_name,
               t.business_name,
               t.function_name,
               t.function_author,
               t.options,
               t.remark,
               t.swagger,
               t.excel_export,
               t.vue_version,
               c.column_id,
               c.column_name,
               c.column_comment,
               c.column_type,
               c.java_type,
               c.java_field,
               c.is_pk,
               c.is_increment,
               c.is_required,
               c.is_insert,
               c.is_edit,
               c.is_list,
               c.is_query,
               c.query_type,
               c.html_type,
               c.dict_type,
               c.sort
        FROM gen_table t
                     LEFT JOIN gen_table_column c ON t.table_id = c.table_id
        ORDER BY c.sort
    </select>

    <insert id="insertGenTable" parameterType="GenTable" useGeneratedKeys="true" keyProperty="tableId">
        INSERT INTO gen_table (
        <if test="tableName != null">
            table_name,
        </if>
        <if test="tableComment != null and tableComment != ''">
            table_comment,
        </if>
        <if test="className != null and className != ''">
            class_name,
        </if>
        <if test="tplCategory != null and tplCategory != ''">
            tpl_category,
        </if>
        <if test="packageName != null and packageName != ''">
            package_name,
        </if>
        <if test="moduleName != null and moduleName != ''">
            module_name,
        </if>
        <if test="businessName != null and businessName != ''">
            business_name,
        </if>
        <if test="functionName != null and functionName != ''">
            function_name,
        </if>
        <if test="functionAuthor != null and functionAuthor != ''">
            function_author,
        </if>
        <if test="genType != null and genType != ''">
            gen_type,
        </if>
        <if test="genPath != null and genPath != ''">
            gen_path,
        </if>
        <if test="remark != null and remark != ''">
            remark,
        </if>
        <if test="swagger != null">
            swagger,
        </if>
        <if test="excelExport != null">
            excel_export,
        </if>
        <if test="vueVersion != null">
            vue_version,
        </if>
        <if test="createBy != null and createBy != ''">
            create_by,
        </if>
        create_time, update_time) values (
        <if test="tableName != null">
            #{tableName,jdbcType=VARCHAR},
        </if>
        <if test="tableComment != null and tableComment != ''">
            #{tableComment,jdbcType=VARCHAR},
        </if>
        <if test="className != null and className != ''">
            #{className,jdbcType=VARCHAR},
        </if>
        <if test="tplCategory != null and tplCategory != ''">
            #{tplCategory,jdbcType=VARCHAR},
        </if>
        <if test="packageName != null and packageName != ''">
            #{packageName,jdbcType=VARCHAR},
        </if>
        <if test="moduleName != null and moduleName != ''">
            #{moduleName,jdbcType=VARCHAR},
        </if>
        <if test="businessName != null and businessName != ''">
            #{businessName,jdbcType=VARCHAR},
        </if>
        <if test="functionName != null and functionName != ''">
            #{functionName,jdbcType=VARCHAR},
        </if>
        <if test="functionAuthor != null and functionAuthor != ''">
            #{functionAuthor,jdbcType=VARCHAR},
        </if>
        <if test="genType != null and genType != ''">
            #{genType,jdbcType=VARCHAR},
        </if>
        <if test="genPath != null and genPath != ''">
            #{genPath,jdbcType=VARCHAR},
        </if>
        <if test="remark != null and remark != ''">
            #{remark,jdbcType=VARCHAR},
        </if>
        <if test="swagger != null">
            #{swagger,jdbcType=INTEGER},
        </if>
        <if test="excelExport != null">
            #{excelExport,jdbcType=INTEGER},
        </if>
        <if test="vueVersion != null">
            #{vueVersion,jdbcType=INTEGER},
        </if>
        <if test="createBy != null and createBy != ''">
            #{createBy,jdbcType=VARCHAR},
        </if>
        NOW(), NOW()
                )
    </insert>

    <update id="updateGenTable" parameterType="GenTable">
        UPDATE gen_table
        <set>
            <if test="tableName != null">
                table_name = #{tableName,jdbcType=VARCHAR},
            </if>
            <if test="tableComment != null and tableComment != ''">
                table_comment = #{tableComment,jdbcType=VARCHAR},
            </if>
            <if test="subTableName != null">
                sub_table_name = #{subTableName,jdbcType=VARCHAR},
            </if>
            <if test="subTableFkName != null">
                sub_table_fk_name = #{subTableFkName,jdbcType=VARCHAR},
            </if>
            <if test="className != null and className != ''">
                class_name = #{className,jdbcType=VARCHAR},
            </if>
            <if test="functionAuthor != null and functionAuthor != ''">
                function_author = #{functionAuthor,jdbcType=VARCHAR},
            </if>
            <if test="genType != null and genType != ''">
                gen_type = #{genType,jdbcType=VARCHAR},
            </if>
            <if test="genPath != null and genPath != ''">
                gen_path = #{genPath,jdbcType=VARCHAR},
            </if>
            <if test="tplCategory != null and tplCategory != ''">
                tpl_category = #{tplCategory},
            </if>
            <if test="packageName != null and packageName != ''">
                package_name = #{packageName},
            </if>
            <if test="moduleName != null and moduleName != ''">
                module_name = #{moduleName},
            </if>
            <if test="businessName != null and businessName != ''">
                business_name = #{businessName,jdbcType=VARCHAR},
            </if>
            <if test="functionName != null and functionName != ''">
                function_name = #{functionName,jdbcType=VARCHAR},
            </if>
            <if test="options != null and options != ''">
                options = #{options,jdbcType=LONGVARCHAR},
            </if>
            <if test="updateBy != null and updateBy != ''">
                update_by = #{updateBy,jdbcType=VARCHAR},
            </if>
            <if test="remark != null">
                remark = #{remark,jdbcType=VARCHAR},
            </if>
            <if test="swagger != null">
                swagger = #{swagger,jdbcType=INTEGER},
            </if>
            <if test="excelExport != null">
                excel_export = #{excelExport,jdbcType=INTEGER},
            </if>
            <if test="vueVersion != null">
                vue_version = #{vueVersion,jdbcType=INTEGER},
            </if>
            update_time = NOW()
        </set>
        WHERE table_id = #{tableId,jdbcType=BIGINT}
    </update>

    <delete id="deleteGenTableByIds" parameterType="Long">
        DELETE
        FROM gen_table WHERE table_id IN
        <foreach collection="array" item="tableId" open="(" separator="," close=")">
            #{tableId,jdbcType=BIGINT}
        </foreach>
    </delete>

    <insert id="batchInsert" keyColumn="table_id" keyProperty="tableId" parameterType="map" useGeneratedKeys="true">
        <!--@mbg.generated-->
        INSERT INTO gen_table
                (`table_name`, table_comment, sub_table_name, sub_table_fk_name, class_name, tpl_category,
                 package_name, module_name, business_name, function_name, function_author, gen_type,
                 gen_path, `options`, create_by, create_time, update_by, update_time, remark, swagger, excel_export, vue_version)
                VALUES
        <foreach collection="list" item="item" separator=",">
            (#{item.tableName,jdbcType=VARCHAR}, #{item.tableComment,jdbcType=VARCHAR}, #{item.subTableName,jdbcType=VARCHAR},
             #{item.subTableFkName,jdbcType=VARCHAR}, #{item.className,jdbcType=VARCHAR}, #{item.tplCategory,jdbcType=VARCHAR},
             #{item.packageName,jdbcType=VARCHAR}, #{item.moduleName,jdbcType=VARCHAR}, #{item.businessName,jdbcType=VARCHAR},
             #{item.functionName,jdbcType=VARCHAR}, #{item.functionAuthor,jdbcType=VARCHAR},
             #{item.genType,jdbcType=CHAR}, #{item.genPath,jdbcType=VARCHAR}, #{item.options,jdbcType=VARCHAR},
             #{item.createBy,jdbcType=VARCHAR}, NOW(), #{item.updateBy,jdbcType=VARCHAR},
             NOW(), #{item.remark,jdbcType=VARCHAR}, #{item.swagger,jdbcType=INTEGER}, #{item.excelExport,jdbcType=INTEGER}, #{item.vueVersion,jdbcType=INTEGER})
        </foreach>
    </insert>

    <select id="selectCountByTableName" resultType="int">
        SELECT COUNT(*)
        FROM gen_table
        WHERE `table_name` = #{tableName,jdbcType=VARCHAR}
    </select>

</mapper>